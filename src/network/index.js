import axios from "axios";
import {baseUrl} from '@/network/urlConfig'
const req = axios.create({
  // baseURL: "http://192.168.1.95:8888",
  // baseURL: "http://192.168.1.83:8888",
  baseURL: baseUrl,
  timeout: 5000, 
  // headers:{
  //   token:''
  // },
});

// 请求拦截器
req.interceptors.request.use((config) => {
  //需要携带token带给服务器
  return config;
});
// 响应拦截器
req.interceptors.response.use(
  (res) => {
    return res;
  },
  (err) => {
    console.log(err)
  }
);

export default req;
