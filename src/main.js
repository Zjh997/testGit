import Vue from "vue";
import App from "./App.vue";
import store from "@/store";
import router from "./router/index";
import antdv from "ant-design-vue";
import "ant-design-vue/dist/antd.css";
import "@/assets/iconfont/iconfont.css";
import req from '@/network/index'
import "xe-utils";
import VXETable from "vxe-table";
// import "vxe-table/lib/style.css";
// 修改vxe-table样式
import "@/style/vxeTable.scss"
// import element from './ElementUI'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
// import XEUtils from 'xe-utils'
// import VXEUtils from 'vxe-utils'
VXETable.setup({
  icon: {
    TABLE_EDIT: "",
  },
});
Vue.use(VXETable);
Vue.use(ElementUI) 
Vue.use(antdv);
Vue.config.productionTip = false;

new Vue({
  beforeCreate() {
    Vue.prototype.$bus = this;
    Vue.prototype.request = req
  },
  render: (h) => h(App),
  router,
  store,
}).$mount("#app");
