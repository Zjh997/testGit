//引入Vue
import Vue from "vue";
//引入路由组件
import Router from "vue-router";
//使用路由
Vue.use(Router);

import LayOut from "@/page/layout.vue";
import Simulation from "@/page/simulation.vue";
import TeleControl from "@/page/telecontrol.vue";
import TeleMetry from "@/page/telemetry.vue";
import BreakDown from "@/page/breakdown.vue";

const routes = [
  {
    path: "/",
    name:'layout',
    redirect: "/Layout",
    component: LayOut,
    children: [
      {
        path: "",
        name:'simulation',
        redirect: "simulation",
        meta: {
          keepAlive: true // 需要被缓存
        }
      },
      {
        path: "simulation",
        name:'simulation',
        component: Simulation,
        meta: {
          keepAlive: true // 需要被缓存
        }
      },
      {
        path: "telecontrol",
        name:'telecontrol',
        component: TeleControl,
        meta: {
          keepAlive: true // 需要被缓存
        }
      },
      {
        path: "telemetry",
        name:'telemetry',
        component: TeleMetry,
        meta: {
          keepAlive: true // 需要被缓存
        }
      },
      {
        path: "breakdown",
        name:'breakdown',
        component: BreakDown,
        meta: {
          keepAlive: true // 需要被缓存
        }
      },
    ],
  },
];
const router = new Router({
  routes,
  mode: "hash",
});

export default router;
