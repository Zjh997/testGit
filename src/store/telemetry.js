const state = {
  tableContent:[]
};
const mutations = {
  SAVEFILEDATA(state, fileData) {
    state.tableContent = fileData;
  },
};
const actions = {
  saveFileData({ commit },fileData) {
    commit("SAVEFILEDATA", fileData);
  },
};
const getters = {
  tableContent(state) {
    return state.tableContent || [];
  },
};
export default {
  state,
  mutations,
  actions,
  getters,
};
