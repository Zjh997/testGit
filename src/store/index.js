import Vuex from 'vuex'
import Vue from 'vue'
Vue.use(Vuex)
import telemetry from "./telemetry";
export default new Vuex.Store({
  modules: {
    telemetry
  },
});
