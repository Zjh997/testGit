const path = require("path"); //引入path模块
function resolve(dir) {
  return path.join(__dirname, dir); //path.join(__dirname)设置绝对路径
}
module.exports = {
  productionSourceMap: false,
  // 关闭ESLINT校验工具
  lintOnSave: false,
  chainWebpack: (config) => {
    config.resolve.alias
        .set('@',resolve('./src'))
        .set('components',resolve('./src/components'))
        .set('views',resolve('src/views'))
        .set('assets',resolve('src/assets'))
  },
    devServer: {
      // proxy: {
      //   "/api": {
      //     target: "http://111.206.116.214:8080/",
      //   },
      // },
      proxy:{
        '/api':{
            target:'http://192.168.1.141:8888',
            secure:false,
            changeOrigin:true,
            pathRewrite:{
                '^/api':''
            }
        }
      }
    },
};
